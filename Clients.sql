USE [Sughd]
GO

/****** Object:  View [dbo].[Clients]    Script Date: 12/21/2014 23:12:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- Octopus Microfinance Software
-- Copyright © - «Оctopus Microfinance» LLC & OXUS DEVELOPMENT NETWORK – 2006-2013
-- LICENSE: This source file is subject to version 1.0 of the Octopus Microfinance License.
-- Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
-- If you did not receive a copy of the Octopus Microfinance License and are unable to obtain it through the web, please send a note to info@octopusnetwork.org so we can mail you a copy immediately.
ALTER VIEW [dbo].[Clients]
AS
-- Persons
SELECT p.id, 'I' AS code, p.first_name + ' ' + p.last_name AS name,
d.name AS district, d.id AS district_id, ISNULL(t.city, 'Not set') AS city, ISNULL(t.[address], 'Not set') AS address_cl
FROM dbo.Persons p
LEFT JOIN Tiers t ON t.id=p.id
LEFT JOIN Districts d ON d.id=t.district_id
-- Groups
UNION ALL
SELECT g.id, 'G' AS code, g.name AS name,
d.name AS district, d.id AS district_id, ISNULL(t.city, 'Not set') AS city, ISNULL(t.[address], 'Not set') AS address_cl
FROM dbo.Groups g
INNER JOIN PersonGroupBelonging AS pgb ON pgb.group_id = g.id 
INNER JOIN Persons AS p ON p.id = pgb.person_id
INNER JOIN tiers t ON t.id = p.id
LEFT JOIN Districts AS d ON d.id = t.district_id
WHERE pgb.is_leader = 1
AND pgb.currently_in = 1
-- Corporates
UNION ALL
SELECT id, 'C', name,
'Not set' AS district, 0 AS district_id, 'Not set' AS city, 'Not set' AS address_cl
FROM dbo.Corporates
-- Villages
UNION ALL
SELECT id, 'V', name,
'Not set' AS district, 0 AS district_id, 'Not set' AS city, 'Not set' AS address_cl
FROM dbo.Villages



GO


