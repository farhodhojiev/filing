﻿using System.Windows.Forms;
using Octopus.Extensions;

namespace Octopus.Extension.Filing
{
    public class Menu:IMenu
    {
        public ExtensionMenuItem[] GetItems()
        {
            var item = new ToolStripMenuItem
            {
                Name = "mnuFilingExtension",
                Text = "Filing"
            };
            item.Click += (o, e) =>
            {
                var frm = new FilingList();
                frm.Show();
            };
            var extensionItem = new ExtensionMenuItem
            {
                InsertAfter = "miSearchClient",
                MenuItem = item
            };
            return new[] { extensionItem };
        }
    }
}
