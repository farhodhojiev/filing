﻿namespace Octopus.Extension.Filing
{
    partial class Form_edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_edit));
            this.comboBox_users = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox_reason = new System.Windows.Forms.ComboBox();
            this.listView_events = new System.Windows.Forms.ListView();
            this.columnHeader_who = new System.Windows.Forms.ColumnHeader();
            this.columnHeader_reason = new System.Windows.Forms.ColumnHeader();
            this.columnHeader_reason_comment = new System.Windows.Forms.ColumnHeader();
            this.columnHeader_d_t = new System.Windows.Forms.ColumnHeader();
            this.columnHeader_d_r = new System.Windows.Forms.ColumnHeader();
            this.columnHeader_renewal = new System.Windows.Forms.ColumnHeader();
            this.columnHeader_renewal_comments = new System.Windows.Forms.ColumnHeader();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.textBox_renewal_comment = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel_returned = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_add_comment = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel_renewal = new System.Windows.Forms.Panel();
            this.panel_notreturned = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.label_client = new System.Windows.Forms.Label();
            this.panel_returned.SuspendLayout();
            this.panel_renewal.SuspendLayout();
            this.panel_notreturned.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBox_users
            // 
            this.comboBox_users.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_users.FormattingEnabled = true;
            this.comboBox_users.Location = new System.Drawing.Point(3, 28);
            this.comboBox_users.Name = "comboBox_users";
            this.comboBox_users.Size = new System.Drawing.Size(214, 21);
            this.comboBox_users.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(68, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Receiptor";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(302, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Reason";
            // 
            // comboBox_reason
            // 
            this.comboBox_reason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_reason.FormattingEnabled = true;
            this.comboBox_reason.Location = new System.Drawing.Point(223, 28);
            this.comboBox_reason.Name = "comboBox_reason";
            this.comboBox_reason.Size = new System.Drawing.Size(214, 21);
            this.comboBox_reason.TabIndex = 5;
            // 
            // listView_events
            // 
            this.listView_events.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader_who,
            this.columnHeader_reason,
            this.columnHeader_reason_comment,
            this.columnHeader_d_t,
            this.columnHeader_d_r,
            this.columnHeader_renewal,
            this.columnHeader_renewal_comments,
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.listView_events.FullRowSelect = true;
            this.listView_events.GridLines = true;
            this.listView_events.Location = new System.Drawing.Point(12, 230);
            this.listView_events.Name = "listView_events";
            this.listView_events.Size = new System.Drawing.Size(790, 222);
            this.listView_events.TabIndex = 13;
            this.listView_events.UseCompatibleStateImageBehavior = false;
            this.listView_events.View = System.Windows.Forms.View.Details;
            this.listView_events.SelectedIndexChanged += new System.EventHandler(this.listView_events_SelectedIndexChanged);
            // 
            // columnHeader_who
            // 
            this.columnHeader_who.Text = "Receiptor";
            this.columnHeader_who.Width = 207;
            // 
            // columnHeader_reason
            // 
            this.columnHeader_reason.Text = "Reason";
            this.columnHeader_reason.Width = 130;
            // 
            // columnHeader_reason_comment
            // 
            this.columnHeader_reason_comment.Text = "Comment";
            this.columnHeader_reason_comment.Width = 90;
            // 
            // columnHeader_d_t
            // 
            this.columnHeader_d_t.Text = "Date of receiving";
            this.columnHeader_d_t.Width = 97;
            // 
            // columnHeader_d_r
            // 
            this.columnHeader_d_r.Text = "Date of return";
            this.columnHeader_d_r.Width = 86;
            // 
            // columnHeader_renewal
            // 
            this.columnHeader_renewal.Text = "Extension date";
            // 
            // columnHeader_renewal_comments
            // 
            this.columnHeader_renewal_comments.Text = "Extension comments";
            this.columnHeader_renewal_comments.Width = 116;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Deleted";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Delete date";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Delete user";
            // 
            // textBox_renewal_comment
            // 
            this.textBox_renewal_comment.Location = new System.Drawing.Point(120, 10);
            this.textBox_renewal_comment.Name = "textBox_renewal_comment";
            this.textBox_renewal_comment.Size = new System.Drawing.Size(336, 20);
            this.textBox_renewal_comment.TabIndex = 12;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(462, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(317, 26);
            this.button1.TabIndex = 14;
            this.button1.Text = "Return";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.Location = new System.Drawing.Point(462, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(317, 26);
            this.button2.TabIndex = 15;
            this.button2.Text = "Extension";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel_returned
            // 
            this.panel_returned.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_returned.Controls.Add(this.button3);
            this.panel_returned.Controls.Add(this.label3);
            this.panel_returned.Controls.Add(this.textBox_add_comment);
            this.panel_returned.Controls.Add(this.comboBox_users);
            this.panel_returned.Controls.Add(this.label1);
            this.panel_returned.Controls.Add(this.comboBox_reason);
            this.panel_returned.Controls.Add(this.label2);
            this.panel_returned.Enabled = false;
            this.panel_returned.Location = new System.Drawing.Point(12, 39);
            this.panel_returned.Name = "panel_returned";
            this.panel_returned.Size = new System.Drawing.Size(790, 57);
            this.panel_returned.TabIndex = 16;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button3.Location = new System.Drawing.Point(615, 23);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(164, 26);
            this.button3.TabIndex = 17;
            this.button3.Text = "Add";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(500, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Comment";
            // 
            // textBox_add_comment
            // 
            this.textBox_add_comment.Location = new System.Drawing.Point(443, 28);
            this.textBox_add_comment.Name = "textBox_add_comment";
            this.textBox_add_comment.Size = new System.Drawing.Size(166, 20);
            this.textBox_add_comment.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(3, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Extension comment";
            // 
            // panel_renewal
            // 
            this.panel_renewal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_renewal.Controls.Add(this.label4);
            this.panel_renewal.Controls.Add(this.button2);
            this.panel_renewal.Controls.Add(this.textBox_renewal_comment);
            this.panel_renewal.Enabled = false;
            this.panel_renewal.Location = new System.Drawing.Point(12, 118);
            this.panel_renewal.Name = "panel_renewal";
            this.panel_renewal.Size = new System.Drawing.Size(790, 37);
            this.panel_renewal.TabIndex = 19;
            // 
            // panel_notreturned
            // 
            this.panel_notreturned.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_notreturned.Controls.Add(this.button1);
            this.panel_notreturned.Enabled = false;
            this.panel_notreturned.Location = new System.Drawing.Point(12, 177);
            this.panel_notreturned.Name = "panel_notreturned";
            this.panel_notreturned.Size = new System.Drawing.Size(790, 35);
            this.panel_notreturned.TabIndex = 20;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(12, 458);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(126, 23);
            this.button4.TabIndex = 21;
            this.button4.Text = "Delete Last Event";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label_client
            // 
            this.label_client.AutoSize = true;
            this.label_client.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_client.Location = new System.Drawing.Point(9, 9);
            this.label_client.Name = "label_client";
            this.label_client.Size = new System.Drawing.Size(66, 24);
            this.label_client.TabIndex = 22;
            this.label_client.Text = "label5";
            // 
            // Form_edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 488);
            this.Controls.Add(this.label_client);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.panel_notreturned);
            this.Controls.Add(this.panel_renewal);
            this.Controls.Add(this.panel_returned);
            this.Controls.Add(this.listView_events);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_edit";
            this.Text = "Edit";
            this.Load += new System.EventHandler(this.Form_edit_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_edit_FormClosing);
            this.panel_returned.ResumeLayout(false);
            this.panel_returned.PerformLayout();
            this.panel_renewal.ResumeLayout(false);
            this.panel_renewal.PerformLayout();
            this.panel_notreturned.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox_users;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox_reason;
        private System.Windows.Forms.ListView listView_events;
        private System.Windows.Forms.ColumnHeader columnHeader_who;
        private System.Windows.Forms.ColumnHeader columnHeader_reason;
        private System.Windows.Forms.ColumnHeader columnHeader_d_t;
        private System.Windows.Forms.ColumnHeader columnHeader_d_r;
        private System.Windows.Forms.TextBox textBox_renewal_comment;
        private System.Windows.Forms.ColumnHeader columnHeader_renewal;
        private System.Windows.Forms.ColumnHeader columnHeader_renewal_comments;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel_returned;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_add_comment;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel_renewal;
        private System.Windows.Forms.Panel panel_notreturned;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ColumnHeader columnHeader_reason_comment;
        private System.Windows.Forms.Label label_client;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
    }
}