﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Octopus.CoreDomain;
using System.Data.SqlClient;

namespace Octopus.Extension.Filing
{
    public partial class FilingList
    {
        public void LoadFilters_all()
        {
            //////////////////////////////////////////////////// load districts
            string query = "SELECT name FROM dbo.districts where deleted=0";
            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                var reader = cmd.ExecuteReader();
                comboBox_district.Items.Add("All");
                while (reader.Read())
                {
                    comboBox_district.Items.Add(reader.GetString(reader.GetOrdinal("name")));
                }
            }
            //////////////////////////////////////////////////// load loan officers
            query = "SELECT u.first_name+SPACE(1)+u.last_name AS LO FROM dbo.users u where role_code='LOF' OR role_code='ADMIN' ORDER BY u.last_name";
            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                var reader = cmd.ExecuteReader();
                comboBox_LO.Items.Add("All");
                while (reader.Read())
                {
                    comboBox_LO.Items.Add(reader.GetString(reader.GetOrdinal("LO")));
                }
            }
            //////////////////////////////////////////////////// load credit statuses
            query = "SELECT status_name FROM dbo.statuses";
            using(var conn = DatabaseConnection.GetConnection()){
                var cmd = new SqlCommand(query, conn);
                var reader = cmd.ExecuteReader();
                comboBox_status.Items.Add("All");
                while (reader.Read())
                {
                    comboBox_status.Items.Add(reader.GetString(reader.GetOrdinal("status_name")));
                }
            }
            //////////////////////////////////////////////////// who have
            query = "SELECT u.first_name+SPACE(1)+u.last_name AS whohave FROM dbo.users u  ORDER BY u.last_name";
            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                var reader = cmd.ExecuteReader();
                comboBox_whohave.Items.Add("All");
                while (reader.Read())
                {
                    comboBox_whohave.Items.Add(reader.GetString(reader.GetOrdinal("whohave")));
                }
            }
            //////////////////////////////////////////////////// load reasons
            query = "SELECT reason_value FROM dbo.Ex_Filing_Reason";
            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                var reader = cmd.ExecuteReader();
                comboBox_reason.Items.Add("All");
                while (reader.Read())
                {
                    comboBox_reason.Items.Add(reader.GetString(reader.GetOrdinal("reason_value")));
                }
            }
        }
    }
}