﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using Octopus.Extensions;

namespace Octopus.Extension.Filing
{
    [Export(typeof(IExtension))]
    public class Extension : IExtension
    {
        #region IExtension Members

        public string GetMeta(string key)
        {
            var meta = new Dictionary<string, string>
            {
                {"Name", "Filing Extension"},
                {"OctopusVersion", "v4.9.0"},
                {"Vendor", "OTJ Solutions FHR"}
            };
            return meta.ContainsKey(key) ? meta[key] : string.Empty;
        }


        public Guid Guid
        {
            get { return new Guid("1DB0EE32-3158-4598-8CA9-7FFB8BE3AA46"); }
        }


        public object QueryInterface(Type t)
        {
            if (null == t) return null;
            var map = new Dictionary<Type, Type>
            {
                {typeof(IMenu), typeof(Menu)}
            };

            var targetType = map.ContainsKey(t) ? map[t] : null;
            if (null == targetType) return null;
            var ctor = targetType.GetConstructor(new Type[] { });
            return null == ctor ? null : ctor.Invoke(new object[] { });
        }

        #endregion
    }
}