﻿namespace Octopus.Extension.Filing
{
    partial class FilingList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FilingList));
            this.button_add_folder = new System.Windows.Forms.Button();
            this.listView_all = new System.Windows.Forms.ListView();
            this.columnHeader_id = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_code = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_clname = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_disdate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_whoreceipt = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_receiptdate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_lo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_district = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_status = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_contract_code = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_who_have = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_dateofgive = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_reason = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_comment = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader_latedays = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBox_latedays = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox_reason = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox_whohave = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox_status = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox_LO = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox_district = new System.Windows.Forms.ComboBox();
            this.textBox_search = new System.Windows.Forms.TextBox();
            this.button_search = new System.Windows.Forms.Button();
            this.label_count = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.max_of_result = new System.Windows.Forms.NumericUpDown();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.max_of_result)).BeginInit();
            this.SuspendLayout();
            // 
            // button_add_folder
            // 
            this.button_add_folder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_add_folder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_add_folder.Location = new System.Drawing.Point(12, 449);
            this.button_add_folder.Name = "button_add_folder";
            this.button_add_folder.Size = new System.Drawing.Size(126, 43);
            this.button_add_folder.TabIndex = 0;
            this.button_add_folder.Text = "Add credit file";
            this.button_add_folder.UseVisualStyleBackColor = true;
            this.button_add_folder.Click += new System.EventHandler(this.button_add_folder_Click);
            // 
            // listView_all
            // 
            this.listView_all.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView_all.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader_id,
            this.columnHeader_code,
            this.columnHeader_clname,
            this.columnHeader_disdate,
            this.columnHeader_whoreceipt,
            this.columnHeader_receiptdate,
            this.columnHeader_lo,
            this.columnHeader_district,
            this.columnHeader_status,
            this.columnHeader_contract_code,
            this.columnHeader_who_have,
            this.columnHeader_dateofgive,
            this.columnHeader_reason,
            this.columnHeader_comment,
            this.columnHeader_latedays});
            this.listView_all.FullRowSelect = true;
            this.listView_all.GridLines = true;
            this.listView_all.LabelWrap = false;
            this.listView_all.Location = new System.Drawing.Point(4, 34);
            this.listView_all.MultiSelect = false;
            this.listView_all.Name = "listView_all";
            this.listView_all.Size = new System.Drawing.Size(634, 409);
            this.listView_all.TabIndex = 3;
            this.listView_all.UseCompatibleStateImageBehavior = false;
            this.listView_all.View = System.Windows.Forms.View.Details;
            this.listView_all.SelectedIndexChanged += new System.EventHandler(this.listView_all_SelectedIndexChanged);
            this.listView_all.DoubleClick += new System.EventHandler(this.listView_all_DoubleClick);
            // 
            // columnHeader_id
            // 
            this.columnHeader_id.Text = "ID";
            this.columnHeader_id.Width = 0;
            // 
            // columnHeader_code
            // 
            this.columnHeader_code.Text = "Code";
            this.columnHeader_code.Width = 80;
            // 
            // columnHeader_clname
            // 
            this.columnHeader_clname.Text = "Client name";
            this.columnHeader_clname.Width = 120;
            // 
            // columnHeader_disdate
            // 
            this.columnHeader_disdate.Text = "Disbursed date";
            this.columnHeader_disdate.Width = 90;
            // 
            // columnHeader_whoreceipt
            // 
            this.columnHeader_whoreceipt.Text = "User registration";
            // 
            // columnHeader_receiptdate
            // 
            this.columnHeader_receiptdate.Text = "Date of registration";
            this.columnHeader_receiptdate.Width = 90;
            // 
            // columnHeader_lo
            // 
            this.columnHeader_lo.Text = "Loan Officer";
            this.columnHeader_lo.Width = 77;
            // 
            // columnHeader_district
            // 
            this.columnHeader_district.Text = "District";
            // 
            // columnHeader_status
            // 
            this.columnHeader_status.Text = "Status";
            this.columnHeader_status.Width = 45;
            // 
            // columnHeader_contract_code
            // 
            this.columnHeader_contract_code.Text = "Contract Code";
            this.columnHeader_contract_code.Width = 150;
            // 
            // columnHeader_who_have
            // 
            this.columnHeader_who_have.Text = "Receiptor";
            // 
            // columnHeader_dateofgive
            // 
            this.columnHeader_dateofgive.Text = "Date of receiving";
            // 
            // columnHeader_reason
            // 
            this.columnHeader_reason.Text = "Reason";
            // 
            // columnHeader_comment
            // 
            this.columnHeader_comment.Text = "Comment";
            // 
            // columnHeader_latedays
            // 
            this.columnHeader_latedays.Text = "Use days";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.max_of_result);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.comboBox_latedays);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.comboBox_reason);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.comboBox_whohave);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.comboBox_status);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.comboBox_LO);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.comboBox_district);
            this.groupBox1.Location = new System.Drawing.Point(644, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(188, 489);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filters";
            // 
            // comboBox_latedays
            // 
            this.comboBox_latedays.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_latedays.FormattingEnabled = true;
            this.comboBox_latedays.Items.AddRange(new object[] {
            "All",
            "1-5",
            "6-10",
            "11-30",
            ">30"});
            this.comboBox_latedays.Location = new System.Drawing.Point(8, 350);
            this.comboBox_latedays.Name = "comboBox_latedays";
            this.comboBox_latedays.Size = new System.Drawing.Size(174, 21);
            this.comboBox_latedays.TabIndex = 17;
            this.comboBox_latedays.SelectedIndexChanged += new System.EventHandler(this.comboBox_latedays_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 332);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Use days";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // comboBox_reason
            // 
            this.comboBox_reason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_reason.FormattingEnabled = true;
            this.comboBox_reason.Location = new System.Drawing.Point(8, 293);
            this.comboBox_reason.Name = "comboBox_reason";
            this.comboBox_reason.Size = new System.Drawing.Size(174, 21);
            this.comboBox_reason.TabIndex = 15;
            this.comboBox_reason.SelectedIndexChanged += new System.EventHandler(this.comboBox_reason_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 274);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Reason";
            // 
            // comboBox_whohave
            // 
            this.comboBox_whohave.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_whohave.FormattingEnabled = true;
            this.comboBox_whohave.Location = new System.Drawing.Point(8, 234);
            this.comboBox_whohave.Name = "comboBox_whohave";
            this.comboBox_whohave.Size = new System.Drawing.Size(174, 21);
            this.comboBox_whohave.TabIndex = 13;
            this.comboBox_whohave.SelectedIndexChanged += new System.EventHandler(this.comboBox_whohave_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 216);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Receiptor";
            // 
            // comboBox_status
            // 
            this.comboBox_status.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_status.FormattingEnabled = true;
            this.comboBox_status.Location = new System.Drawing.Point(8, 171);
            this.comboBox_status.Name = "comboBox_status";
            this.comboBox_status.Size = new System.Drawing.Size(174, 21);
            this.comboBox_status.TabIndex = 11;
            this.comboBox_status.SelectedIndexChanged += new System.EventHandler(this.comboBox_status_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 152);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Contract status";
            // 
            // comboBox_LO
            // 
            this.comboBox_LO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_LO.FormattingEnabled = true;
            this.comboBox_LO.Location = new System.Drawing.Point(8, 104);
            this.comboBox_LO.Name = "comboBox_LO";
            this.comboBox_LO.Size = new System.Drawing.Size(174, 21);
            this.comboBox_LO.TabIndex = 9;
            this.comboBox_LO.SelectedIndexChanged += new System.EventHandler(this.comboBox_LO_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Loan Officers";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Districts";
            // 
            // comboBox_district
            // 
            this.comboBox_district.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_district.FormattingEnabled = true;
            this.comboBox_district.Location = new System.Drawing.Point(8, 44);
            this.comboBox_district.Name = "comboBox_district";
            this.comboBox_district.Size = new System.Drawing.Size(174, 21);
            this.comboBox_district.TabIndex = 6;
            this.comboBox_district.SelectedIndexChanged += new System.EventHandler(this.comboBox_district_SelectedIndexChanged);
            // 
            // textBox_search
            // 
            this.textBox_search.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_search.Location = new System.Drawing.Point(4, 8);
            this.textBox_search.Name = "textBox_search";
            this.textBox_search.Size = new System.Drawing.Size(506, 20);
            this.textBox_search.TabIndex = 6;
            this.textBox_search.TextChanged += new System.EventHandler(this.textBox_search_TextChanged);
            // 
            // button_search
            // 
            this.button_search.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_search.Location = new System.Drawing.Point(516, 5);
            this.button_search.Name = "button_search";
            this.button_search.Size = new System.Drawing.Size(122, 23);
            this.button_search.TabIndex = 7;
            this.button_search.Text = "Search";
            this.button_search.UseVisualStyleBackColor = true;
            this.button_search.Click += new System.EventHandler(this.button_search_Click);
            // 
            // label_count
            // 
            this.label_count.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label_count.AutoSize = true;
            this.label_count.Location = new System.Drawing.Point(603, 449);
            this.label_count.Name = "label_count";
            this.label_count.Size = new System.Drawing.Size(35, 13);
            this.label_count.TabIndex = 8;
            this.label_count.Text = "Count";
            this.label_count.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 387);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Max of Result";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // max_of_result
            // 
            this.max_of_result.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.max_of_result.Location = new System.Drawing.Point(9, 404);
            this.max_of_result.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.max_of_result.Name = "max_of_result";
            this.max_of_result.Size = new System.Drawing.Size(75, 20);
            this.max_of_result.TabIndex = 19;
            this.max_of_result.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.max_of_result.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // FilingList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMinSize = new System.Drawing.Size(100, 100);
            this.ClientSize = new System.Drawing.Size(835, 498);
            this.Controls.Add(this.label_count);
            this.Controls.Add(this.button_search);
            this.Controls.Add(this.textBox_search);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.listView_all);
            this.Controls.Add(this.button_add_folder);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FilingList";
            this.Text = "Filing List";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FilingList_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.max_of_result)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_add_folder;
        private System.Windows.Forms.ColumnHeader columnHeader_id;
        private System.Windows.Forms.ListView listView_all;
        private System.Windows.Forms.ColumnHeader columnHeader_clname;
        private System.Windows.Forms.ColumnHeader columnHeader_disdate;
        private System.Windows.Forms.ColumnHeader columnHeader_lo;
        private System.Windows.Forms.ColumnHeader columnHeader_district;
        private System.Windows.Forms.ColumnHeader columnHeader_status;
        private System.Windows.Forms.ColumnHeader columnHeader_receiptdate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox_search;
        private System.Windows.Forms.Button button_search;
        private System.Windows.Forms.ColumnHeader columnHeader_who_have;
        private System.Windows.Forms.ColumnHeader columnHeader_dateofgive;
        private System.Windows.Forms.ColumnHeader columnHeader_reason;
        private System.Windows.Forms.ColumnHeader columnHeader_comment;
        private System.Windows.Forms.ColumnHeader columnHeader_latedays;
        private System.Windows.Forms.ColumnHeader columnHeader_whoreceipt;
        private System.Windows.Forms.ColumnHeader columnHeader_contract_code;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox_district;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox_LO;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox_status;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox_whohave;
        private System.Windows.Forms.ComboBox comboBox_reason;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox_latedays;
        private System.Windows.Forms.Label label_count;
        private System.Windows.Forms.ColumnHeader columnHeader_code;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown max_of_result;



    }
}