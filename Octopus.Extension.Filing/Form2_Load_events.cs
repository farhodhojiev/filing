﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Octopus.CoreDomain;
using System.Data.SqlClient;

namespace Octopus.Extension.Filing
{
    public class ComboboxItem //this class for index of listbox
    {
        public string Text { get; set; }
        public object Value { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }

    public partial class Form_edit : Form
    {
        public void Load_Events()
        {
            //////////////////////////////////////////////////// load client name and contract
            //int list_selected_index = FilingListForm_edit.list_selected_index;
            //label_client.Text = list_selected_index.ToString();
            string query = @"SELECT TOP 1 fl.filing_code+SPACE(3)+cl.name+SPACE(3)+c.contract_code AS label
                FROM dbo.Ex_Filing_List AS fl
                LEFT JOIN dbo.Clients cl ON cl.id=fl.tiers_id
                LEFT JOIN dbo.Contracts c ON c.id=
	                (SELECT MAX(c2.id) FROM dbo.Contracts c2
	                LEFT JOIN dbo.Credit cr2 ON cr2.id=c2.id
	                LEFT JOIN dbo.Tiers t2 ON t2.id=c2.tiers_id
	                WHERE t2.id=fl.tiers_id)
                WHERE fl.id=@id";
            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", FilingListForm_edit.list_selected_index);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    label_client.Text = reader.GetString(reader.GetOrdinal("label"));
                }
            }
            //////////////////////////////////////////////////// load users
            query = "SELECT u.id AS id, u.first_name+SPACE(1)+u.last_name AS all_users FROM dbo.users u where u.deleted=0  ORDER BY u.last_name";
            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ComboboxItem item = new ComboboxItem();
                    item.Text = reader.GetString(reader.GetOrdinal("all_users"));
                    item.Value = reader.GetInt32(reader.GetOrdinal("id"));
                    comboBox_users.Items.Add(item);
                }
            }
            //////////////////////////////////////////////////// load reason
            query = "SELECT id, reason_value FROM dbo.Ex_Filing_Reason WHERE is_deleted=0";
            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ComboboxItem item = new ComboboxItem();
                    item.Text = reader.GetString(reader.GetOrdinal("reason_value"));
                    item.Value = reader.GetInt32(reader.GetOrdinal("id"));
                    comboBox_reason.Items.Add(item);
                }
            }
            //////////////////////////////////////////////////// load history
            listView_events.Items.Clear();
            query = @"SELECT u.first_name+SPACE(1)+u.last_name AS who,
                fr.reason_value,
                ISNULL(fh.comment, '') comment,
                ISNULL(CONVERT(VARCHAR(19), fh.date_of_give, 120), '') date_of_give,
                ISNULL(CONVERT(VARCHAR(19), fh.date_of_return, 120), '') date_of_return,
                ISNULL(CONVERT(VARCHAR(19), fh.renewal_date, 120), '') renewal_date,
                ISNULL(fh.renewal_comment, '') renewal_comment,
                fh.is_deleted,
                ISNULL(CONVERT(VARCHAR(19), fh.delete_date, 120), '') delete_date,
                ISNULL(d_u.last_name+SPACE(1)+d_u.first_name, '') delete_who
                FROM dbo.Ex_Filing_History fh
                LEFT JOIN dbo.Users u ON u.id=fh.[user_id]
                LEFT JOIN dbo.Ex_Filing_Reason fr ON fh.reason_id=fr.id
                LEFT JOIN dbo.Users d_u ON d_u.id=fh.delete_user_id
                WHERE fh.Ex_Filing_List_id=@id
                ORDER BY fh.id";
            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", FilingListForm_edit.list_selected_index);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    
                    Int32 is_deleted = reader.GetInt32(reader.GetOrdinal("is_deleted"));
                    string[] row = { 
                                   reader.GetString(reader.GetOrdinal("who")),
                                   reader.GetString(reader.GetOrdinal("reason_value")),
                                   reader.GetString(reader.GetOrdinal("comment")),
                                   reader.GetString(reader.GetOrdinal("date_of_give")),
                                   reader.GetString(reader.GetOrdinal("date_of_return")),
                                   reader.GetString(reader.GetOrdinal("renewal_date")),
                                   reader.GetString(reader.GetOrdinal("renewal_comment")),
                                   is_deleted.ToString(),
                                   reader.GetString(reader.GetOrdinal("delete_date")),
                                   reader.GetString(reader.GetOrdinal("delete_who"))
                                   };
                    var listViewItemss = new ListViewItem(row);
                    if (is_deleted == 1)
                    {
                        System.Drawing.Color col = Color.LightGray;
                        listViewItemss.BackColor = col;
                    }
                    listView_events.Items.Add(listViewItemss);
                }
            }
            //////////////////////////////////////////////////// disable/enable controls
            query = @"SELECT TOP 1 fl.id,
                ISNULL((SELECT TOP 1 fh.returned FROM dbo.Ex_Filing_History fh 
                WHERE fh.is_deleted=0 AND fh.Ex_Filing_List_id=fl.id ORDER BY fh.id DESC), 1) AS returned,
                ISNULL((SELECT TOP 1 fh.renewal FROM dbo.Ex_Filing_History fh 
                WHERE fh.is_deleted=0 AND fh.Ex_Filing_List_id=fl.id ORDER BY fh.id DESC), 2) AS renewal
                FROM dbo.Ex_Filing_List fl
                WHERE fl.id=@id";
            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", FilingListForm_edit.list_selected_index);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Int32 returned = reader.GetInt32(reader.GetOrdinal("returned"));
                    Int32 renewal = reader.GetInt32(reader.GetOrdinal("renewal"));
                    if (returned == 1)
                    {
                        panel_returned.Enabled = true;
                        panel_notreturned.Enabled = false;
                    }
                    else {
                        panel_returned.Enabled = false;
                        panel_notreturned.Enabled = true;
                    }
                    if (renewal != 2 && returned==0)
                    {
                        panel_renewal.Enabled = true;
                    }
                    else
                    {
                        panel_renewal.Enabled = false;
                    }
                }
            }
        }
    }
}