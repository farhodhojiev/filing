﻿namespace Octopus.Extension.Filing
{
    partial class Form_client_search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("zxc");
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("asd");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_client_search));
            this.textBox_search = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.listView_search = new System.Windows.Forms.ListView();
            this.columnHeader_id = new System.Windows.Forms.ColumnHeader();
            this.columnHeader_name = new System.Windows.Forms.ColumnHeader();
            this.columnHeader_active = new System.Windows.Forms.ColumnHeader();
            this.columnHeader_passport = new System.Windows.Forms.ColumnHeader();
            this.columnHeader_district = new System.Windows.Forms.ColumnHeader();
            this.columnHeader_city = new System.Windows.Forms.ColumnHeader();
            this.columnHeader_cycle = new System.Windows.Forms.ColumnHeader();
            this.columnHeader_status = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // textBox_search
            // 
            this.textBox_search.Location = new System.Drawing.Point(12, 13);
            this.textBox_search.Name = "textBox_search";
            this.textBox_search.Size = new System.Drawing.Size(531, 20);
            this.textBox_search.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(558, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 27);
            this.button1.TabIndex = 1;
            this.button1.Text = "Search";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // listView_search
            // 
            this.listView_search.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader_id,
            this.columnHeader_name,
            this.columnHeader_active,
            this.columnHeader_passport,
            this.columnHeader_district,
            this.columnHeader_city,
            this.columnHeader_cycle,
            this.columnHeader_status});
            this.listView_search.FullRowSelect = true;
            this.listView_search.GridLines = true;
            this.listView_search.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2});
            this.listView_search.LabelWrap = false;
            this.listView_search.Location = new System.Drawing.Point(12, 42);
            this.listView_search.MultiSelect = false;
            this.listView_search.Name = "listView_search";
            this.listView_search.Size = new System.Drawing.Size(648, 304);
            this.listView_search.TabIndex = 2;
            this.listView_search.UseCompatibleStateImageBehavior = false;
            this.listView_search.View = System.Windows.Forms.View.Details;
            this.listView_search.SelectedIndexChanged += new System.EventHandler(this.listView_search_SelectedIndexChanged);
            this.listView_search.DoubleClick += new System.EventHandler(this.listView_search_DoubleClick);
            // 
            // columnHeader_id
            // 
            this.columnHeader_id.Text = "id";
            this.columnHeader_id.Width = 0;
            // 
            // columnHeader_name
            // 
            this.columnHeader_name.Text = "Client Name";
            this.columnHeader_name.Width = 192;
            // 
            // columnHeader_active
            // 
            this.columnHeader_active.Text = "Active";
            this.columnHeader_active.Width = 44;
            // 
            // columnHeader_passport
            // 
            this.columnHeader_passport.Text = "Passport";
            this.columnHeader_passport.Width = 109;
            // 
            // columnHeader_district
            // 
            this.columnHeader_district.Text = "District";
            this.columnHeader_district.Width = 99;
            // 
            // columnHeader_city
            // 
            this.columnHeader_city.Text = "City";
            this.columnHeader_city.Width = 75;
            // 
            // columnHeader_cycle
            // 
            this.columnHeader_cycle.Text = "Cycle";
            this.columnHeader_cycle.Width = 39;
            // 
            // columnHeader_status
            // 
            this.columnHeader_status.Text = "Status";
            this.columnHeader_status.Width = 85;
            // 
            // Form_client_search
            // 
            this.AcceptButton = this.button1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(682, 358);
            this.Controls.Add(this.listView_search);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox_search);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_client_search";
            this.Text = "Search Client with Loans";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_search;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListView listView_search;
        private System.Windows.Forms.ColumnHeader columnHeader_name;
        private System.Windows.Forms.ColumnHeader columnHeader_active;
        private System.Windows.Forms.ColumnHeader columnHeader_passport;
        private System.Windows.Forms.ColumnHeader columnHeader_district;
        private System.Windows.Forms.ColumnHeader columnHeader_city;
        private System.Windows.Forms.ColumnHeader columnHeader_cycle;
        private System.Windows.Forms.ColumnHeader columnHeader_status;
        private System.Windows.Forms.ColumnHeader columnHeader_id;
    }
}