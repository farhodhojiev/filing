﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Octopus.Extensions;
using Octopus.CoreDomain;


namespace Octopus.Extension.Filing
{
    public partial class FilingList : Form
    {
        public int list_selected_index; // need to send selected items from list

        public FilingList()
        {
            InitializeComponent();
        }



        private void FilingList_Load(object sender, EventArgs e)
        {
            table_initialize();
            LoadFilters_all();
            LoadList_all();
        }


        private void button_add_folder_Click(object sender, EventArgs e)
        {
            var frm_search = new Form_client_search();
            frm_search.FilingListForm = this; //for connection with other forms
            frm_search.ShowDialog();
        }

        private void listView_all_DoubleClick(object sender, EventArgs e)
        {
            var frm_edit = new Form_edit();
            list_selected_index = int.Parse(listView_all.SelectedItems[0].SubItems[0].Text);
            frm_edit.FilingListForm_edit = this; //for connection with other forms
            frm_edit.ShowDialog();
        }

        private void listView_all_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button_search_Click(object sender, EventArgs e)
        {
            LoadList_all_with_search();
        }

        private void textBox_search_TextChanged(object sender, EventArgs e)
        {
            LoadList_all_with_search();
        }

        private void comboBox_district_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadList_all_with_search();
        }

        private void comboBox_LO_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadList_all_with_search();
        }

        private void comboBox_status_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadList_all_with_search();
        }

        private void comboBox_whohave_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadList_all_with_search();
        }

        private void comboBox_reason_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadList_all_with_search();
        }

        private void comboBox_latedays_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadList_all_with_search();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }


        
    }
}
