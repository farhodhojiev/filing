﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Octopus.CoreDomain;

namespace Octopus.Extension.Filing
{
    public partial class FilingList
    {
        public void LoadList_all_with_search()
        {
            listView_all.Items.Clear(); //clear all

            string txt_search = "(name like '%" + textBox_search.Text + "%' OR filing_code like '%" + textBox_search.Text + "%' OR contract_code like '%" + textBox_search.Text + "%') ";
                        
            string combo_district; //////////////
            if (comboBox_district.Text=="All"){
                combo_district = "AND district like '%%' ";
            }else{
                combo_district = "AND district like '%" + comboBox_district.Text + "%' ";
            }
            
            string combo_lo; ///////////////
            if (comboBox_LO.Text == "All")
            {
                combo_lo = "AND LO like '%%' ";
            }else{
                combo_lo = "AND LO like '%" + comboBox_LO.Text + "%' ";
            }

            string combo_status; ////////////////////
            if (comboBox_status.Text == "All")
            {
                combo_status = "AND status_name like '%%' ";
            }
            else
            {
                combo_status = "AND status_name like '%" + comboBox_status.Text + "%' ";
            }

            string combo_whohave; ////////////////////
            if (comboBox_whohave.Text == "All")
            {
                combo_whohave = "AND who_have like '%%' ";
            }
            else
            {
                combo_whohave = "AND who_have like '%" + comboBox_whohave.Text + "%' ";
            }

            string combo_reason; ////////////////////
            if (comboBox_reason.Text == "All")
            {
                combo_reason = "AND reason like '%%' ";
            }
            else
            {
                combo_reason = "AND reason like '%" + comboBox_reason.Text + "%' ";
            }

            string combo_latedays; ////////////////////
            if(comboBox_latedays.Text == "1-5")
            {
                combo_latedays = "AND late_day>=1 AND late_day<=5 ";
            }
            else if (comboBox_latedays.Text == "6-10")
            {
                combo_latedays = "AND late_day>=6 AND late_day<=10 ";
            }
            else if (comboBox_latedays.Text == "11-30")
            {
                combo_latedays = "AND late_day>=11 AND late_day<=30 ";
            }
            else if (comboBox_latedays.Text == ">30")
            {
                combo_latedays = "AND late_day > 30 ";
            }
            else { combo_latedays = " "; }

            folders_t.DefaultView.Sort = "id DESC";
            DataRow[] result = folders_t.Select(txt_search + combo_district + combo_lo + combo_whohave + combo_reason + combo_status + combo_latedays);

            Int16 n_of_row_for_show = Decimal.ToInt16(max_of_result.Value);
            foreach (DataRow row in result.Take(n_of_row_for_show))
            {

                string[] row2 = {row[0].ToString(),
                                row[1].ToString(),
                                row[2].ToString(),
                                row[3].ToString(),
                                row[4].ToString(),
                                row[5].ToString(),
                                row[6].ToString(),
                                row[7].ToString(),
                                row[8].ToString(),
                                row[9].ToString(),

                                row[10].ToString(),
                                row[11].ToString(),
                                row[12].ToString(),
                                row[13].ToString(),
                                row[14].ToString()
                               };
                var listViewItemss = new ListViewItem(row2);
                System.Drawing.Color col = System.Drawing.ColorTranslator.FromHtml(row[15].ToString()); //bgcolor from hex color
                listViewItemss.ForeColor = col;

                
                int ld = int.Parse(row[14].ToString());
                if (ld > 0)
                {
                    if (ld > 235) { ld = 235; }
                    System.Drawing.Color redColor = Color.FromArgb(222, 255-ld-20, 255-ld-20); //fcolor late color
                    listViewItemss.BackColor = redColor;
                    //listViewItemss.Font = new Font(listViewItemss.Font, FontStyle.Bold);
                }
                listView_all.Items.Add(listViewItemss);
            }
            label_count.Text = listView_all.Items.Count.ToString(); //count of records
            //listView_all.Sort
        }
    }
}