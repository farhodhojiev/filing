﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Octopus.CoreDomain;
using System.Data.SqlClient;

namespace Octopus.Extension.Filing
{
    public partial class Form_client_search : Form
    {

        public FilingList FilingListForm { get; set; } //connect with main form for call method

        public Form_client_search()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            const string query = @"SELECT TOP 50
                t.id AS id,
                MAX(con.id) con_id,
                MAX(COALESCE(p.first_name+SPACE(1)+p.last_name, g.name, c.name)) AS client_name,
                MAX(CONVERT(int, t.active)) AS active,
                MAX(p.identification_data) AS passport,
                MAX(s.status_name) AS status_name,
                MAX(cl.district) AS district,
                MAX(t.city) AS city,
                MAX(t.loan_cycle) AS cycle
                FROM dbo.Contracts con
                LEFT JOIN dbo.Tiers t ON t.id=con.tiers_id
                LEFT JOIN dbo.Clients cl ON cl.id=t.id
                LEFT JOIN dbo.Persons p ON t.id=p.id
                LEFT JOIN dbo.Groups g ON t.id=g.id
                LEFT JOIN dbo.Corporates c ON t.id=c.id
                LEFT JOIN dbo.Statuses s ON s.id=con.status
                INNER JOIN dbo.FilteredCreditContracts(@id, 0, 0) fcc ON fcc.id = con.id
                WHERE p.identification_data like '%'+@ssearch+'%' OR
                COALESCE(p.first_name+SPACE(1)+p.last_name, g.name, c.name) like '%'+@ssearch+'%'
                GROUP BY t.id";
            var conn = DatabaseConnection.GetConnection();
            var cmd = new SqlCommand((query), conn);
            cmd.Parameters.AddWithValue("@id", User.CurrentUser.Id);
            cmd.Parameters.AddWithValue("@ssearch", textBox_search.Text);
            var reader = cmd.ExecuteReader();

            listView_search.Items.Clear();
            while (reader.Read())
            {
                var id = reader.GetInt32(reader.GetOrdinal("id"));
                var client_name = reader.GetString(reader.GetOrdinal("client_name"));
                var active = reader.GetInt32(reader.GetOrdinal("active"));
                var passport = reader.IsDBNull(reader.GetOrdinal("passport"))? null : reader.GetString(reader.GetOrdinal("passport"));
                var status_name = reader.GetString(reader.GetOrdinal("status_name"));
                var district = reader.GetString(reader.GetOrdinal("district"));
                var city = reader.IsDBNull(reader.GetOrdinal("city")) ? null : reader.GetString(reader.GetOrdinal("city"));
                var cycle = reader.GetInt32(reader.GetOrdinal("cycle"));

                string[] row = {id.ToString(), client_name, active.ToString(), passport ?? " ",
                               district, city ?? " ", cycle.ToString(), status_name};

                var listViewItem = new ListViewItem(row);
                listView_search.Items.Add(listViewItem);
            }
        }

        private void listView_search_DoubleClick(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to add this item?", "Add Folder", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                const string query = @"INSERT INTO dbo.ex_filing_list(tiers_id, [user_id]) VALUES (@tiers_id, @id)";
                var conn = DatabaseConnection.GetConnection();
                var cmd = new SqlCommand((query), conn);
                cmd.Parameters.AddWithValue("@id", User.CurrentUser.Id);
                cmd.Parameters.AddWithValue("@tiers_id", int.Parse(listView_search.SelectedItems[0].SubItems[0].Text));
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch
                {
                    MessageBox.Show("This entry already have", "OTJ Filing");
                    throw;
                }
               
                FilingListForm.LoadList_all();
                this.Close();
               
            }
            else
            {
                this.Activate();
            }   
        }

        private void listView_search_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}
