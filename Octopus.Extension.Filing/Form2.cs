﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Octopus.CoreDomain;
using System.Data.SqlClient;

namespace Octopus.Extension.Filing
{
    public partial class Form_edit : Form
    {
        public FilingList FilingListForm_edit { get; set; } //connect with main form for call method

        public Form_edit()
        {
            InitializeComponent();
        }


        private void Form_edit_Load(object sender, EventArgs e)
        {
            Load_Events();
        }

        void Form_edit_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            FilingListForm_edit.LoadList_all();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string query = @"INSERT INTO dbo.Ex_Filing_History ([Ex_Filing_List_id], [user_id], [add_user_id], [date_of_give], [reason_id], [comment])
                VALUES (@filing_id, @who_give_id, @add_user_id, GETDATE(), @reason_id, @comment)";
            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                try
                {
                    int who_give_id = int.Parse((comboBox_users.SelectedItem as ComboboxItem).Value.ToString());
                    int reason_id = int.Parse((comboBox_reason.SelectedItem as ComboboxItem).Value.ToString());
                    if (MessageBox.Show("Are you sure you want to add?", "Give Folder", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        cmd.Parameters.AddWithValue("@filing_id", FilingListForm_edit.list_selected_index);
                        cmd.Parameters.AddWithValue("@who_give_id", who_give_id);
                        cmd.Parameters.AddWithValue("@add_user_id", User.CurrentUser.Id);
                        cmd.Parameters.AddWithValue("@reason_id", reason_id);
                        cmd.Parameters.AddWithValue("@comment", textBox_add_comment.Text);
                        cmd.ExecuteNonQuery();
                    }
                }
                catch
                {
                    MessageBox.Show("Entry all data");
                    throw;
                }
                Load_Events();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string query = @"UPDATE dbo.Ex_Filing_History SET renewal=1, renewal_date=GETDATE(), renewal_comment=@renewal_comment
                WHERE id=(SELECT MAX(id) FROM dbo.Ex_Filing_History WHERE is_deleted=0 AND Ex_Filing_List_id=@filing_id)";
            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                if (MessageBox.Show("Are you sure you want to renewal?", "Renewal Folder", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    cmd.Parameters.AddWithValue("@filing_id", FilingListForm_edit.list_selected_index);
                    cmd.Parameters.AddWithValue("@renewal_comment", textBox_renewal_comment.Text);
                    cmd.ExecuteNonQuery();
                }
                Load_Events();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string query = @"UPDATE dbo.Ex_Filing_History SET returned=1, date_of_return=GETDATE(), return_user_id=@user_id
            WHERE id=(SELECT MAX(id) FROM dbo.Ex_Filing_History WHERE is_deleted=0 AND Ex_Filing_List_id=@filing_id)";
            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                if (MessageBox.Show("Are you sure you want to return?", "Return Folder", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    cmd.Parameters.AddWithValue("@filing_id", FilingListForm_edit.list_selected_index);
                    cmd.Parameters.AddWithValue("@user_id", User.CurrentUser.Id);
                    cmd.ExecuteNonQuery();
                }
                Load_Events();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string query = @"SELECT TOP 1 fl.id,
                ISNULL((SELECT TOP 1 fh.returned FROM dbo.Ex_Filing_History fh 
                WHERE fh.is_deleted=0 AND fh.Ex_Filing_List_id=fl.id ORDER BY fh.id DESC), 1) AS returned,
                ISNULL((SELECT TOP 1 fh.renewal FROM dbo.Ex_Filing_History fh 
                WHERE fh.is_deleted=0 AND fh.Ex_Filing_List_id=fl.id ORDER BY fh.id DESC), 1) AS renewal
                FROM dbo.Ex_Filing_List fl
                WHERE fl.id=@id";
            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", FilingListForm_edit.list_selected_index);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Int32 returned = reader.GetInt32(reader.GetOrdinal("returned"));
                    Int32 renewal = reader.GetInt32(reader.GetOrdinal("renewal"));

                    if (MessageBox.Show("Are you sure you want delete event?", "Delete Folder", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        if (returned == 1) //delete returned
                        {
                            query = @"UPDATE dbo.Ex_Filing_History SET returned=0, date_of_return=NULL, delete_date=GETDATE(), delete_user_id=@user_id
                                        WHERE id=(SELECT MAX(id) FROM dbo.Ex_Filing_History WHERE is_deleted=0 AND Ex_Filing_List_id=@filing_id)";
                            using (var conn2 = DatabaseConnection.GetConnection())
                            {
                                var cmd2 = new SqlCommand(query, conn2);
                                cmd2.Parameters.AddWithValue("@filing_id", FilingListForm_edit.list_selected_index);
                                cmd2.Parameters.AddWithValue("@user_id", User.CurrentUser.Id);
                                cmd2.ExecuteNonQuery();
                            }
                        }
                        else if (renewal == 1) //delete renewal
                        {
                            query = @"UPDATE dbo.Ex_Filing_History SET renewal=0, renewal_date=NULL, renewal_comment='', delete_date=GETDATE(), delete_user_id=@user_id
                                        WHERE id=(SELECT MAX(id) FROM dbo.Ex_Filing_History WHERE is_deleted=0 AND Ex_Filing_List_id=@filing_id)";
                            using (var conn2 = DatabaseConnection.GetConnection())
                            {
                                var cmd2 = new SqlCommand(query, conn2);
                                cmd2.Parameters.AddWithValue("@filing_id", FilingListForm_edit.list_selected_index);
                                cmd2.Parameters.AddWithValue("@user_id", User.CurrentUser.Id);
                                cmd2.ExecuteNonQuery();
                            }
                        }
                        else //delete give
                        {
                            query = @"UPDATE dbo.Ex_Filing_History SET is_deleted=1, delete_date=GETDATE(), delete_user_id=@user_id
                                        WHERE id=(SELECT MAX(id) FROM dbo.Ex_Filing_History WHERE is_deleted=0 AND Ex_Filing_List_id=@filing_id)";
                            using (var conn2 = DatabaseConnection.GetConnection())
                            {
                                var cmd2 = new SqlCommand(query, conn2);
                                cmd2.Parameters.AddWithValue("@filing_id", FilingListForm_edit.list_selected_index);
                                cmd2.Parameters.AddWithValue("@user_id", User.CurrentUser.Id);
                                cmd2.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
            Load_Events();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void listView_events_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
