﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Octopus.CoreDomain;
using System.Data.SqlClient;


namespace Octopus.Extension.Filing
{
    public partial class FilingList
    {
        //SqlDataReader reader;
        DataTable folders_t = new DataTable("folders");
        public void table_initialize()
        {
            folders_t.Columns.Add(new DataColumn("id", typeof(int)));
            folders_t.Columns.Add(new DataColumn("filing_code", typeof(string)));
            folders_t.Columns.Add(new DataColumn("name", typeof(string)));
            folders_t.Columns.Add(new DataColumn("dis_date", typeof(string)));
            folders_t.Columns.Add(new DataColumn("OO", typeof(string)));
            folders_t.Columns.Add(new DataColumn("date_of_receipt", typeof(string)));
            folders_t.Columns.Add(new DataColumn("LO", typeof(string)));
            folders_t.Columns.Add(new DataColumn("district", typeof(string)));
            folders_t.Columns.Add(new DataColumn("status_name", typeof(string)));
            folders_t.Columns.Add(new DataColumn("contract_code", typeof(string)));

            folders_t.Columns.Add(new DataColumn("who_have", typeof(string)));
            folders_t.Columns.Add(new DataColumn("date_of_give", typeof(string)));
            folders_t.Columns.Add(new DataColumn("reason", typeof(string)));
            folders_t.Columns.Add(new DataColumn("comment", typeof(string)));
            folders_t.Columns.Add(new DataColumn("late_day", typeof(int)));
            folders_t.Columns.Add(new DataColumn("r_color", typeof(string)));
        }
        
        public void LoadList_all()
        {
            var conn = DatabaseConnection.GetConnection();



            //string sqlTrunc = "TRUNCATE TABLE " + "folders_t";
            //SqlCommand cmd2 = new SqlCommand(sqlTrunc, conn);
            //cmd2.ExecuteNonQuery();
            folders_t.Rows.Clear();

            const string query = @"SELECT fl.id,
                fl.filing_code,
                cl.name,
                u.first_name+SPACE(1)+u.last_name AS OO,
                CONVERT(VARCHAR(19), fl.date_of_receipt, 120) date_of_receipt,
                CONVERT(VARCHAR(10), c.start_date, 120) dis_date,
                u2.first_name+SPACE(1)+u2.last_name AS LO,
                cl.district AS district,
                st.status_name,
                c.contract_code,

                CASE WHEN ISNULL(fh.returned, 0)=1 THEN ' ' ELSE ISNULL(CONVERT(VARCHAR(19), fh.date_of_give, 120), '') END AS date_of_give,
                CASE WHEN ISNULL(fh.returned, 0)=1 THEN ' ' ELSE ISNULL(u3.first_name+SPACE(1)+u3.last_name, '') END AS who_have,
                CASE WHEN ISNULL(fh.returned, 0)=1 THEN ' ' ELSE ISNULL(fr.reason_value, '') END AS reason,
                CASE WHEN ISNULL(fh.returned, 0)=1 THEN ' ' ELSE ISNULL(fh.comment, '') END AS comment,
                CASE WHEN ISNULL(fh.returned, 0)=1 THEN 0 ELSE ISNULL(DATEDIFF(d, fh.date_of_give, GETDATE()), 0) END AS late_day,
                CASE WHEN ISNULL(fh.returned, 0)=1 THEN '#000000' ELSE ISNULL(fr.reason_color, '#000000') END AS r_color

                FROM dbo.Ex_Filing_List fl
                LEFT JOIN dbo.Clients cl ON cl.id=fl.tiers_id
                LEFT JOIN dbo.Tiers t ON t.id=fl.tiers_id
                LEFT JOIN dbo.Users u ON u.id=fl.[user_id]
                LEFT JOIN dbo.Contracts c ON c.id=
                    (SELECT MAX(c2.id) FROM dbo.Contracts c2
                    LEFT JOIN dbo.Credit cr2 ON cr2.id=c2.id
                    LEFT JOIN dbo.Tiers t2 ON t2.id=c2.tiers_id
                    WHERE t2.id=t.id)
                LEFT JOIN dbo.Credit cr ON cr.id=c.id
                LEFT JOIN dbo.Statuses st ON st.id=c.[status]
                LEFT JOIN dbo.Users u2 ON u2.id=cr.loanofficer_id

                LEFT JOIN dbo.Ex_Filing_History fh ON fh.id=
                    (SELECT MAX(fh2.id) FROM dbo.EX_Filing_History fh2
                    WHERE fh2.is_deleted=0 AND fh2.Ex_Filing_List_id=fl.id)
                LEFT JOIN dbo.Ex_Filing_Reason fr ON fr.id=fh.reason_id
                LEFT JOIN dbo.Users u3 ON u3.id=fh.[user_id]

                INNER JOIN dbo.FilteredCreditContracts(@id, 0, 0) fcc ON fcc.id = c.id";

            var cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@id", User.CurrentUser.Id);
            var reader = cmd.ExecuteReader();
            
            while (reader.Read())
            {
                folders_t.Rows.Add(
                reader.GetInt32(reader.GetOrdinal("id")),
                reader.GetString(reader.GetOrdinal("filing_code")),
                reader.GetString(reader.GetOrdinal("name")),
                reader.GetString(reader.GetOrdinal("dis_date")),
                reader.GetString(reader.GetOrdinal("OO")),
                reader.GetString(reader.GetOrdinal("date_of_receipt")),
                reader.GetString(reader.GetOrdinal("LO")),
                reader.GetString(reader.GetOrdinal("district")),
                reader.GetString(reader.GetOrdinal("status_name")),
                reader.GetString(reader.GetOrdinal("contract_code")),

                reader.GetString(reader.GetOrdinal("who_have")),
                reader.GetString(reader.GetOrdinal("date_of_give")),
                reader.GetString(reader.GetOrdinal("reason")),
                reader.GetString(reader.GetOrdinal("comment")),
                reader.GetInt32(reader.GetOrdinal("late_day")),
                reader.GetString(reader.GetOrdinal("r_color")));
            }

            LoadList_all_with_search();
            //MessageBox.Show("asd", "Application Closed!", MessageBoxButtons.OK);
        }
    }
}