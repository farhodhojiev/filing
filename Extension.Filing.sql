CREATE TABLE [dbo].[Ex_Filing_List](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tiers_id] INT NOT NULL UNIQUE,
	[filing_code] NVARCHAR(100),
	[date_of_receipt] DATETIME,
	[user_id] INT NOT NULL,
	
	[district_id] INT
)
---------------------------------------------------------------------------------------------------------

CREATE TABLE [dbo].[Ex_Filing_History](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Ex_Filing_List_id] INT NOT NULL,
	[user_id] INT NOT NULL,
	[add_user_id] INT NOT NULL, --who give file
	[date_of_give] DATETIME NOT NULL,
	[reason_id] NVARCHAR(100) NOT NULL,
	[comment] NVARCHAR(100),

	[renewal] INT NOT NULL,
	[renewal_date] DATETIME,
	[renewal_comment] NVARCHAR(100),
	
	[returned] INT NOT NULL,
	[date_of_return] DATETIME,
	[return_user_id] INT,
	
	[is_deleted] INT NOT NULL,
	[delete_user_id] INT,
	[delete_date] DATETIME
)
ALTER TABLE [dbo].[Ex_Filing_History] ADD  CONSTRAINT [DF_Ex_Filing_History_renewal]  DEFAULT ((0)) FOR [renewal]
ALTER TABLE [dbo].[Ex_Filing_History] ADD  CONSTRAINT [DF_Ex_Filing_History_returned]  DEFAULT ((0)) FOR [returned]
ALTER TABLE [dbo].[Ex_Filing_History] ADD  CONSTRAINT [DF_Ex_Filing_History_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
---------------------------------------------------------------------------------------------------------
CREATE TABLE [dbo].[Ex_Filing_Reason](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[is_deleted] BIT NOT NULL,
	[reason_code] NVARCHAR(5) NOT NULL,
	[reason_value] NVARCHAR(100) NOT NULL,
	[reason_color] VARCHAR(7) NOT NULL,
	[calculate_late_day] INT NOT NULL
)

INSERT INTO [Ex_Filing_Reason] ([is_deleted],[reason_code],[reason_value],[reason_color],[calculate_late_day]) VALUES (0,N'RPD',N'Repeated Disburse','#000000',1);
INSERT INTO [Ex_Filing_Reason] ([is_deleted],[reason_code],[reason_value],[reason_color],[calculate_late_day]) VALUES (0,N'ANL',N'Analysis','#000000',1);
INSERT INTO [Ex_Filing_Reason] ([is_deleted],[reason_code],[reason_value],[reason_color],[calculate_late_day]) VALUES (0,N'DLQ',N'Delinquent','#000000',1);
INSERT INTO [Ex_Filing_Reason] ([is_deleted],[reason_code],[reason_value],[reason_color],[calculate_late_day]) VALUES (0,N'MNT',N'Monitoring','#000000',1);
INSERT INTO [Ex_Filing_Reason] ([is_deleted],[reason_code],[reason_value],[reason_color],[calculate_late_day]) VALUES (0,N'FLP',N'Filing Problem','#000000',1);
INSERT INTO [Ex_Filing_Reason] ([is_deleted],[reason_code],[reason_value],[reason_color],[calculate_late_day]) VALUES (0,N'OTR',N'Other','#000000',1);
INSERT INTO [Ex_Filing_Reason] ([is_deleted],[reason_code],[reason_value],[reason_color],[calculate_late_day]) VALUES (0,N'LAW',N'Law','#000000',1);
INSERT INTO [Ex_Filing_Reason] ([is_deleted],[reason_code],[reason_value],[reason_color],[calculate_late_day]) VALUES (0,N'ARC',N'Archive','#000000',0);

---------------------------------------------------------------------------------------------------------
ALTER TABLE [Ex_Filing_List] ADD PRIMARY KEY (id)

ALTER TABLE [Ex_Filing_History] ADD PRIMARY KEY (id)

ALTER TABLE [dbo].[Ex_Filing_History] WITH NOCHECK ADD  CONSTRAINT [FK_List_History] 
FOREIGN KEY([Ex_Filing_List_id])
REFERENCES [dbo].[Ex_Filing_List] ([id])
GO
---------------------------------------------------------------------------------------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TRIGGER [dbo].[Insert_by_tiersid]
   ON [dbo].[Ex_Filing_List]
   AFTER INSERT
AS
BEGIN
	DECLARE @district_id INT
	DECLARE @district_name NVARCHAR(100)
	SET @district_id=(SELECT district_id FROM dbo.Clients WHERE id=(SELECT tiers_id FROM INSERTED))
	SET @district_name=(SELECT name FROM Districts WHERE id=@district_id)
	
    UPDATE dbo.Ex_Filing_List set date_of_receipt=GETDATE(),
    district_id=@district_id,
    
    filing_code=SUBSTRING(@district_name, 1,4)+
    (SELECT RIGHT('00000' + CAST(
    ((SELECT COUNT(district_id) FROM dbo.Ex_Filing_List
    WHERE district_id=@district_id)+1)
    AS VARCHAR(5)), 5))
    WHERE id=(SELECT ID FROM INSERTED)

END